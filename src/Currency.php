<?php
declare(strict_types=1);

namespace Mantas\Doddle\Src;


class Currency
{
    // all currencies is in cents as php is not good with float mathematics
    const CURRENCYMAP = [
        'JPY' => 0.83,
        'USD' => 92,
    ];
    public function convert(int $sumToConvert, string $currency): int
    {
        $currencyConversion = array_get(self::CURRENCYMAP, $currency, 1);

        $convertedSum = intval($sumToConvert * $currencyConversion);

        return abs($convertedSum);
    }
}