<?php
declare(strict_types=1);

namespace Mantas\Doddle\Src;


class Calc
{
    public function calculateCommission(int $money,  int $percent): int
    {
        $commission = intval($money / $percent / 100);

        return $commission;
    }
}