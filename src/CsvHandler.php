<?php
declare(strict_types=1);

namespace Mantas\Doddle\Src;

class CsvHandler
{
    public function csvStringToArray(array $csvRows): array
    {
        foreach ($csvRows as &$row) {
            $row = explode(',', $row);
        }

        return $csvRows;
    }
}