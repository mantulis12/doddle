<?php

define('BASE_PATH', __DIR__);
date_default_timezone_set('Europe/Vilnius');

require BASE_PATH . '/vendor/autoload.php';

use Mantas\Doddle\Src\Calc;
use Mantas\Doddle\Src\CsvHandler;
use Mantas\Doddle\Src\Currency;

// first we need to read csv
if ($fileName = array_get($argv, '1', null)) {
    $file = file($fileName);
    if (!$file) {
        return;
    }

    $csvRows = (new CsvHandler())->csvStringToArray($file);

    $currencyClass = new Currency();
    $calc = new Calc();

    foreach ($csvRows as $row) {
        $sum = $row[4] * 100;
        $currency = $row[5];
        $percent = $row[1];

        $convertedSum = $currencyClass->convert($sum, $currency);
        echo $calc->calculateCommission($convertedSum, $percent) / 100;
        echo "\n";
    }

    return;
}

echo "File Name Not Found";